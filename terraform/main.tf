terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

# Creds

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud-id
  folder_id = var.folder-id
  zone      = var.zone
}

# Jenkins on Tomcat

resource "yandex_compute_instance" "vm-jenkins" {
  name = "jenkins"

  resources {
    cores  = 4
    memory = 8
  }

  boot_disk {
    initialize_params {
      image_id = var.os-image
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet.id
    ip_address = "10.0.0.11"
    nat        = true
  }

  metadata = {
    user-data = file(var.data-file)
  }
}

# Liferay on WildFly

resource "yandex_compute_instance" "vm-liferay" {
  name = "liferay"

  resources {
    cores  = 4
    memory = 8
  }

  boot_disk {
    initialize_params {
      image_id = var.os-image
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet.id
    ip_address = "10.0.0.12"
    nat        = true
  }

  metadata = {
    user-data = file(var.data-file)
  }
}

# Docker conatiner with PostgreSQL 12

resource "yandex_compute_instance" "vm-postgres" {
  name = "postgres"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = var.os-image
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet.id
    ip_address = "10.0.0.13"
    nat        = true
  }

  metadata = {
    user-data = file(var.data-file)
  }
}

# Configure network

resource "yandex_vpc_network" "network" {
  name = "netcracker-network"
}

resource "yandex_vpc_subnet" "subnet" {
  name           = "netcracker-subnet"
  zone           = var.zone
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}
