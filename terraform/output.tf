output "jenkins_public_ip" {
  value = yandex_compute_instance.vm-jenkins.network_interface.0.nat_ip_address
}

output "liferay_public_ip" {
  value = yandex_compute_instance.vm-liferay.network_interface.0.nat_ip_address
}

output "postgres_public_ip" {
  value = yandex_compute_instance.vm-postgres.network_interface.0.nat_ip_address
}

