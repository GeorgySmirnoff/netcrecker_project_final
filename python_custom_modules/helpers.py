import subprocess
from typing import Optional

from jinja2 import Environment, FileSystemLoader


def execute_command(command: str) -> Optional[str]:
    """Run given command in terminal"""
    print(command)
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = proc.communicate()
    print(error.decode().strip())  # print errors cause there could be warnings for logging
    if not any(i in command for i in ["ssh", "scp"]) and error:
        raise subprocess.CalledProcessError(returncode=proc.returncode, cmd=proc.args, stderr=proc.stderr)
    return output.decode().strip()


def create_file_from_template(path: str, template_name: str, file_to_create: str, params: dict):
    env = Environment(loader=FileSystemLoader(path))
    template = env.get_template(template_name)
    output_from_parsed_template = template.render(params)

    with open(f"{file_to_create}", "w") as f:
        f.writelines(output_from_parsed_template)

