# Ansible for PostgreSQL and Liferay

This directory contains an Ansible script for installing Liferay and PostgreSQL. These playbooks are used by Jenkins for automated infrastructure deployment.

## Playbook for installing PostgreSQL

Playbook for installing PostgreSQL named postgresql_pb.yaml. It configures PostgreSQL in docker container. 

`postgresql_pb.yaml` contains two roles:  
1) `docker`  
2) `postgresql` 

## Playbook for installing Liferay

Playbook for installing Liferay named wildfly-liferay-pb.yaml. It configures Liferay application that is going to be running on wildfly server.

`wildfly-liferay-pb.yaml` contains two roles:  
1) `wildfly`  
2) `liferay`  